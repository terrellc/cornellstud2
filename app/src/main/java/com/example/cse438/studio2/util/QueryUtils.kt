package com.example.cse438.studio2.util

import android.text.TextUtils
import android.util.Log
import com.example.cse438.studio2.model.Offer
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.model.SiteDetail
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset
import kotlin.collections.ArrayList

class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ec2-54-147-241-200.compute-1.amazonaws.com:3000/api/apple_products" // localhost URL

        fun fetchProductData(jsonQueryString: String): ArrayList<Product>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")
            var jsonResponse : String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException){
                Log.e(LogTag, "Problem making the http request")
            }
            return ArrayList<Product>()
        }

        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                }
                else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }

        private fun extractDataFromJson(productJson: String?): ArrayList<Product>? {
            if (TextUtils.isEmpty(productJson)) {
                return null
            }

            val productList = ArrayList<Product>()
            try {
                val baseJsonResponse = JSONArray(productJson)
                for (i in 0 until baseJsonResponse.length()) {
                    val productObject = baseJsonResponse.getJSONObject(i)

                    // Images
                    val images = returnValueOrDefault<JSONArray>(productObject, "images") as JSONArray?
                    val imageArrayList = ArrayList<String>()
                    if (images != null) {
                        for (j in 0 until images.length()) {
                            imageArrayList.add(images.getString(j))
                        }
                    }

                    // Features
                    val features = returnValueOrDefault<JSONObject>(productObject, "features") as JSONObject?
                    val featureDictionary = mutableMapOf<String, Any>()
                    if (features != null) {
                        val featureKeys = features.keys()
                        while (featureKeys.hasNext()) {
                            val key = featureKeys.next()
                            featureDictionary[key] = features[key]
                        }
                    }

                    // Site Details
                    val siteDetails = returnValueOrDefault<JSONArray>(productObject, "sitedetails") as JSONArray?
                    val detailArrayList = ArrayList<SiteDetail>()
                    if (siteDetails != null) {
                        for (j in 0 until siteDetails.length()) {
                            val detail = siteDetails.getJSONObject(j)
                            val offers = returnValueOrDefault<JSONArray>(detail, "latestoffers") as JSONArray?
                            val offerArrayList = ArrayList<Offer>()
                            if (offers != null) {
                                for (k in 0 until offers.length()) {
                                    val offer = offers.getJSONObject(k)
                                    offerArrayList.add(Offer(
                                        returnValueOrDefault<String>(offer, "id") as String,
                                        returnValueOrDefault<String>(offer, "currency") as String,
                                        returnValueOrDefault<Long>(offer, "firstrecorded_at") as Long,
                                        returnValueOrDefault<Double>(offer, "price") as Double,
                                        returnValueOrDefault<Long>(offer, "lastrecorded_at") as Long,
                                        returnValueOrDefault<String>(offer, "seller") as String,
                                        returnValueOrDefault<String>(offer, "condition") as String,
                                        returnValueOrDefault<String>(offer, "availability") as String,
                                        returnValueOrDefault<Int>(offer, "isactive") as Int
                                    ))
                                }
                            }

                            detailArrayList.add(SiteDetail(
                                returnValueOrDefault<String>(detail, "url") as String,
                                returnValueOrDefault<String>(detail, "name") as String,
                                returnValueOrDefault<String>(detail, "sku") as String,
                                returnValueOrDefault<Int>(detail, "recentoffers_count") as Int,
                                offerArrayList
                            ))
                        }
                    }

                    val gtins = returnValueOrDefault<JSONArray>(productObject, "gtins") as JSONArray?
                    var gtinsArray = ArrayList<Long>()
                    if(gtins != null){
                        for(i in 0 until gtins.length()){
                            gtinsArray.add(gtins.getLong(i))
                        }
                    }
                    val variation_secondaryids = returnValueOrDefault<JSONArray>(productObject, "variation_secondaryids") as JSONArray?
                    var secondaryArray = ArrayList<String>()
                    if(variation_secondaryids != null){
                        for(i in 0 until variation_secondaryids.length()){
                            secondaryArray.add(variation_secondaryids.getString(i))
                        }
                    }
                    val sitedetails = returnValueOrDefault<JSONArray>(productObject, "variation_secondaryids") as JSONArray?
                    var siteArray = ArrayList<SiteDetail>()
                    if(sitedetails != null){
                        for(i in 0 until sitedetails!!.length()){
                            siteArray.add(sitedetails[i] as SiteDetail)
                        }
                    }
                    val geo = returnValueOrDefault<JSONArray>(productObject, "variation_secondaryids") as JSONArray?
                    var geoArray = ArrayList<String>()
                    if(geo != null){
                        for(i in 0 until geo.length()){
                            geoArray.add(geo!!.getString(i))
                        }
                    }
                    val messages = returnValueOrDefault<JSONArray>(productObject, "variation_secondaryids") as JSONArray?
                    var messageArray = ArrayList<String>()
                    if(messages != null){
                        for(i in 0 until messages.length()){
                            messageArray.add(variation_secondaryids!!.getString(i))
                        }
                    }

                    //TODO: Finish implementing this method
                    productList.add(Product(
                        // Identifiers
                        returnValueOrDefault<String>(productObject, "id") as String,
                        // Manufacturer Details
                        returnValueOrDefault<String>(productObject, "manufacturer") as String,
                        returnValueOrDefault<String>(productObject, "model") as String,
                        returnValueOrDefault<String>(productObject, "brand") as String,
                        // Category Information
                        returnValueOrDefault<Int>(productObject, "cat_id") as Int,
                        returnValueOrDefault<String>(productObject, "category") as String,
                        // Dimensions and Weight
                        returnValueOrDefault<Double>(productObject, "weight") as Double,
                        returnValueOrDefault<Double>(productObject, "length") as Double,
                        returnValueOrDefault<Double>(productObject, "width") as Double,
                        returnValueOrDefault<Double>(productObject, "height") as Double,
                        // Images
                        returnValueOrDefault<Int>(productObject, "height") as Int,
                        imageArrayList,
                        // Pricing
                        returnValueOrDefault<Double>(productObject, "price") as Double,
                        // Product Information
                        returnValueOrDefault<String>(productObject, "name") as String,
                        returnValueOrDefault<String>(productObject, "description") as String,
                        returnValueOrDefault<String>(productObject, "color") as String,
                        returnValueOrDefault<String>(productObject, "price_currency") as String,
                        featureDictionary,
                        returnValueOrDefault<Int>(productObject, "is_new") as Int,
                        // Important Codes
                        returnValueOrDefault<Long>(productObject, "upc") as Long,
                        returnValueOrDefault<Long>(productObject, "ean") as Long,
                        returnValueOrDefault<String>(productObject, "mpn") as String,
                        gtinsArray,
                        // Key Dates
                        returnValueOrDefault<Long>(productObject, "created_at") as Long,
                        returnValueOrDefault<Long>(productObject, "updated_at") as Long,
                        // Unimportant Data
                        secondaryArray,
                        returnValueOrDefault<String>(productObject, "sem3_id") as String,
                        siteArray,
                        geoArray,
                        messageArray
                        ))

                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the product JSON results", e)
            }

            return productList
        }

        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}