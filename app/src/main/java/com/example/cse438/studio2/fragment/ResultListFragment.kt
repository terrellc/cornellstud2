package com.example.cse438.studio2.fragment

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.cse438.studio2.R
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.viewmodel.ProductViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_result_list.*
import kotlinx.android.synthetic.main.result_list_item.view.*

@SuppressLint("ValidFragment")
class ResultListFragment(context: Context, query: String): Fragment() {
    private var parentContext: Context = context
    private var queryString: String = query
    private lateinit var viewModel: ProductViewModel
    private var resultList: ArrayList<Product> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayText = "Search for: $queryString"
        query_text.text = displayText

        //TODO: Finish implementing below based on HomeFragment
        result_items_list.layoutManager = LinearLayoutManager(parentContext)
        result_items_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        viewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        this.resultList = viewModel.getProductsByQueryText(query_text.text.toString()).value as ArrayList<Product>


    }

    //TODO: Implement the custom adapter below
    inner class resultAdapter: RecyclerView.Adapter<ResultListFragment.resultAdapter.resultViewHolder>(){
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultListFragment.resultAdapter.resultViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.result_list_item, p0, false)
            return resultViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultListFragment.resultAdapter.resultViewHolder, p1: Int) {
            val product = resultList[p1]
            val productImages = product.getImages()
            if (productImages.size == 0) {
                // Do nothing for now
            }
            else {
                Picasso.with(this@ResultListFragment.context).load(productImages[0]).into(p0.productImg)
            }
            p0.productPrice.text = product.getPrice().toString()
            p0.productTitle.text = product.getProductName()

        }
        override fun getItemCount(): Int {
            return resultList.size;
        }
        inner class resultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var productImg: ImageView = itemView.product_img
            var productTitle: TextView = itemView.product_title
            var productPrice: TextView = itemView.product_price
        }
    }

}